﻿Shader "Custom/WorldTiling" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_EmissionColor ("Emission Color", Color) = (0,0,0)
		_EmmisionMap("Emmision Map", 2D) = "white" {}
		_Scale ("Texture Scale Multiplier", Float) = 0.1 
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		fixed4 _Color;
        float _Scale;

		struct Input {
			float2 uv_MainTex;
			float3 worldNormal;
            float3 worldPos;
		};

		half _Glossiness;
		half _Metallic;

		fixed3 _EmissionColor;
		sampler2D _EmmisionMap;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Guess correct planar map from normal. 0.5 is an arbitrary cutoff
			float2 UV;
			// NOTE: assuming no bottom-facing, otherwise use abs()
			if(IN.worldNormal.y>0.5) UV = IN.worldPos.xz; // top
			else if(abs(IN.worldNormal.x)>0.5) UV = IN.worldPos.yz; // side
			else UV = IN.worldPos.xy; // front
 
			// 0.1 is an arbitrary x10 texture size scale 
            half4 c = tex2D (_MainTex, UV* _Scale)* _Color;
            o.Albedo = c.rgb;
            o.Alpha = c.a;

			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;

			half3 e = tex2D(_EmmisionMap, UV* _Scale)* _EmissionColor;

			o.Emission = e.rgb;
			
		}
		ENDCG
	}
	FallBack "Diffuse"
}
