﻿
using Meta;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Despawns : MonoBehaviour {

    private const int sqrmag = 500 * 500; //500 meters away


    [SerializeField]
    private float timer = -1F;

	// Use this for initialization
	void Start () {
        if(timer > 0)
        {
            Invoke("DeleteObject", timer);
        }
        InvokeRepeating("DistanceCheck", 2F, 2F);
	}
	
	void DistanceCheck () {
        Vector3 playerPos = Camera.main.transform.position;
        Vector3 distanceVec = playerPos - transform.position;
        double mag = distanceVec.sqrMagnitude;
        if(mag > sqrmag)
        {
            DeleteObject();
        }
	} 

    void DeleteObject()
    {
        Destroy(transform.root.gameObject,.1F);
    }
}
