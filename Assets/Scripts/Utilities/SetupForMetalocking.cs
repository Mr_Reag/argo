﻿using Meta;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetupForMetalocking : MonoBehaviour {

    private Vector3 initPos;


    [SerializeField]
    private bool activate = false;

    private void Start()
    {
        StartCoroutine(SetupMetaLocking());
    }


    private IEnumerator SetupMetaLocking()
    {
        yield return new WaitForEndOfFrame();
        initPos = transform.position;
        SetLocation();
        var go = gameObject.AddComponent<MetaLocking>();
        go.orbital = false;
        go.hud = true;
        go.useDefaultHUDSettings = false;

        go.hudLockPositionX = true;
        go.hudLockPositionY = false;
        go.hudLockPositionZ = true;

        go.hudLockRotationX = false;
        go.hudLockRotationY = true;
        go.hudLockRotationZ = false;

        yield return null;
    }

    private void Update()
    {
        if (activate) SetLocation();
    }

    void SetLocation()
    {
        Transform root = Camera.main.transform.root;
        Vector3 pos = root.position;



        Vector3 point = Quaternion.LookRotation(root.forward) * initPos;

        point += pos;

        point = new Vector3(point.x, initPos.y, point.z);

        Debug.Log("Moving to: " + point);

        transform.position = point;

        //Set our objects location and rotation to that of the camera, in world space
        transform.rotation = Camera.main.transform.rotation;
        //Find some way to filter out Y component of rotation
        transform.eulerAngles = Vector3.Scale(transform.eulerAngles, Vector3.up); //only allow y component




        /*//Projects the transform onto a sphere dist z away, ceneterd and angled off the headset. It is also offset by the init y


        //Get our position relative to the 0,0,0, where we assume the camera to be
        Vector3 pos = initPos - cameraLoc;
        Vector3 xzAngle = Camera.main.transform.forward;
        Debug.Log("Cam Forward: " + xzAngle.ToString());
        //xzAngle = Vector3.Scale(xzAngle,new Vector3(1, 0, 1)); //remove Y component
        xzAngle.Normalize();
        Debug.Log("Normalized xz angle: " + xzAngle.ToString());

        if(xzAngle.y < 0)
            xzAngle = Vector3.ProjectOnPlane(xzAngle, new Vector3(0,1,0)); //Project onto the y plane of the original position

        

        //Set our objects location and rotation to that of the camera, in world space
        transform.rotation = Camera.main.transform.rotation;
       
        //Find some way to filter out Y component of rotation
        transform.eulerAngles = Vector3.Scale(transform.eulerAngles, new Plane(new Vector3(1,0,1),new Vector3(0,initPos.y,0)).normal); //only allow y component

        transform.position = xzAngle; //move our object to the point we projected

        //Scale our position by forward, up, and right
        //transform.position += Vector3.up * initPos.y; //Set Y to init offset
        //transform.position += xzAngle * initPos.z;    //move the amount we want on the Z access

        Debug.Log("Moving to pos" + transform.position);
        
        //transform.position += transform.up * pos.y;
        //transform.position = new Vector3(transform.position.x,pos.y,transform.position.z); //set Y value to be constant to avoid issues with invisibility

        activate = false;*/
    }
}
