﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;

public class IsSingletonObject : MonoBehaviour {


    /**
     * 
     * READ: THIS MAKES IT SO THAT ONLY ONE OBJECT OF A PARTICULAR NAME WITH THIS SCRIPT CAN EXIST. USE THIS FOR OBJECTS THAT MUST BE UNIQUE AND DONT OVERLOAD NAMES!
     * USE THIS SCRIPT WITH CARE OR ELSE!
     * 
     */ 



    private void Awake()
    {
        GameObject[] roots = SceneManager.GetActiveScene().GetRootGameObjects();
        if (roots.Any(x => x.name.Contains(name))) Destroy(gameObject);
    }
}
