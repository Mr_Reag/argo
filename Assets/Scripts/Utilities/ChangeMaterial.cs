﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ChangeMaterial : MonoBehaviour
{

    [SerializeField]
    private Material changeToOnTrigger;

    private Dictionary<MeshRenderer, Material> dict = new Dictionary<MeshRenderer, Material>();


    public void TriggerMaterialChange()
    {
        var meshRend = GetComponentsInChildren<MeshRenderer>();
        meshRend.ToList().ForEach(y =>
        {

            dict.Add(y, y.material);
            y.material = changeToOnTrigger;
        });
    }


    public void ChangeMaterialBack()
    {
        var meshRend = GetComponentsInChildren<MeshRenderer>();
        meshRend.ToList().ForEach(x =>
        {
            x.material = dict[x];
        });
        dict.Clear();
    }
}
