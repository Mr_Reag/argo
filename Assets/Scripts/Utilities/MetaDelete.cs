﻿using Meta;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Meta.HandInput;

public class MetaDelete : MonoBehaviour {

	/*
     * 
     * This class is used to safely and statically delete metaobjects without causing issues for the SDK
     * 
     */ 


    public static void DeleteMetaObject(GameObject obj)
    {
        var root = obj.transform.root.gameObject;

        var grabbable = root.GetComponentInChildren<Interaction>(); //can we grab this object?

        if(grabbable)
        {
            DisengageAllInteractions(obj);
            root.transform.position = new Vector3(1000, 1000, 1000); //Move away so we ensure that we are no longer grabbing
            Destroy(root, .1F); //wait 10 seconds to be sure no grabs are held, and delete
        } else
        {
            root.transform.position = new Vector3(1000, 1000, 1000);
            Destroy(root,.1F);
        }

    }

    //Disengage all grabs on the object
    public static void DisengageAllInteractions(GameObject obj)
    {
        GameObject metaH = GameObject.Find("MetaHands");
        if (metaH == null) return; //metahands not running -> no interactions
        var hands = metaH.GetComponentsInChildren<Hand>();

        hands.ToList().ForEach(x => x.MarkInvalid());

        /*obj.GetComponentsInChildren<Interaction>().ToList().ForEach(x =>
        {
            if (x)
                hands.ToList().ForEach(h => x.OnGrabDisengaged(h));
        });*/ //disengage all grabs
    }

}
