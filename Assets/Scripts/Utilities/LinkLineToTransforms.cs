﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class LinkLineToTransforms : MonoBehaviour {

    public Transform first;

    public Transform second;


    [SerializeField]
    private LineRenderer line;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (first == null || second == null || line == null) return;

        line.SetPosition(0, first.position);
        line.SetPosition(1, second.position);
	}
}
