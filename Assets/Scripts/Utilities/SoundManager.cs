﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SoundManager : MonoBehaviour {

    [SerializeField]
    private AudioClip[] audioClips;

    private static AudioClip[] staticClips;

    [SerializeField]
    private AudioSource source;

    private static AudioSource staticSource;
    private static SoundManager singleton;

    static bool invalid = true;


    private void Awake()
    {
        if (audioClips.Length == 0)
        {
            Destroy(this);
            return;
        }

        bool setup = SetupSingleton(this);
        if(!setup)
        {
            Destroy(this);
            return;
        }

        if (source == null)
        {
            source = gameObject.AddComponent<AudioSource>();
            source.volume = .25F;
        }

        staticClips = audioClips;
        staticSource = source;

        invalid = false;
    }


    public static void PlayAudioClip(string clipName)
    {
        if (invalid)
        {
            Debug.Log("Audio singleton not setup!");
            return;
        }

        AudioClip clip = staticClips.First(x => x.name.Contains(clipName));
        staticSource.PlayOneShot(clip);
    }

    //On its own to allow for locking
    static readonly object audiolock = new object();
    private static bool SetupSingleton(SoundManager caller)
    {
        lock(audiolock)
        {
            if (singleton != null) return false;
            singleton = caller;
            return true;
        } 
    }
}
