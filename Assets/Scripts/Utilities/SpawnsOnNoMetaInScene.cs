﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



/**
 * NOTE! THIS SCRIPT RUNS LATE IN THE EXECUTION ORDER FOR SAFTY SAKE. MAKE SURE TO CHECK EDIT->PROJECT SETTINGS->SCRIPT EXECUTION ORDER
 */ 
public class SpawnsOnNoMetaInScene : MonoBehaviour {


    [SerializeField]
    private GameObject[] prefabsToSpawn;

    private void Start()
    {
        if(SceneControllerManager.IsManagerSetup())
        {
            DestroyImmediate(gameObject);
            return;
        }
        if (SceneControllerManager.IsSceneControllerSetup())
        {
            DestroyImmediate(gameObject);
            return;
        }
        InstantiatePrefabs();
    }

    //different function to prevent the resources from being loaded at start, which causes a resource crash in the SDK
    void InstantiatePrefabs()
    {
        for (int i = 0; i < prefabsToSpawn.Length; i++)
        {
            var g = Instantiate(prefabsToSpawn[i]);
            //Rename to base prefab name. This gets around errors when people use GameObject.Find('g(Clone)')
            g.name = prefabsToSpawn[i].name;
        }
        Destroy(gameObject);
    }


}
