﻿using Meta.HandInput;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireCannon : MonoBehaviour {

    public GameObject ballPrefab; //make sure this is a rigid body
    public float force = 10F;
    public int cooldownFrames = 600;
    private int cooldown = 0;



    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (cooldown > 0) cooldown--;
        //FireBall();
    }

  

    public void FireBall()
    {
        if (cooldown != 0) return;
        cooldown = cooldownFrames;
        Vector3 direction = new Vector3(0, 0, force); //normalized vector position in the Z direction
        Vector3 vel = gameObject.transform.TransformVector(direction); //transform into a useful velocity
        GameObject spawn = Instantiate(ballPrefab);
        spawn.transform.position = gameObject.transform.TransformPoint(gameObject.transform.localPosition + new Vector3(0, 0, 1.5F)); //set position
        Rigidbody rb = spawn.GetComponent<Rigidbody>();
        rb.velocity = vel;
    }
}
