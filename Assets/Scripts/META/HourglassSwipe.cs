﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Meta.HandInput;

public class HourglassSwipe : MonoBehaviour {




    public void Swipe(HandFeature feature)
    {
        ModularTurntableInteraction turntable = GetComponentInChildren<ModularTurntableInteraction>();
        float angle = turntable.tableAngle;

        int ang = Mathf.Abs(Mathf.RoundToInt(angle) % 360);
        ang /= 90;

        if (ang == 3)
        {
            ResetController.PauseScene();
        }
        if (ang == 0)
        {
            ResetController.PlayScene();
        }
        if (ang == 2)
        {
            ResetController.ResetScene();
            turntable.RotateTo(270);
        }
        if (ang == 1)
        {
            turntable.RotateTo(180);
            ResetController.ResetScene();
            turntable.RotateDelay(270,2);
        }
    }
}
