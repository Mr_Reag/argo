﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallButtonSpawner : MonoBehaviour {

    public GameObject prefab;
    private bool pressed = false;
    public float distanceAhead;

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnPress()
    {
        if (pressed) return;
        pressed = true;
        GameObject spawn = Instantiate(prefab);
        Vector3 pos = gameObject.transform.localPosition;
        pos.y += distanceAhead;
        pos = gameObject.transform.TransformPoint(pos);
        spawn.transform.position = pos;
        Debug.Log("Spawned prefab");
    }

    public void OnRemoveHand()
    {
        pressed = false;
    }
}
