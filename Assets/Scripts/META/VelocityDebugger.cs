﻿using Meta;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class VelocityDebugger : MonoBehaviour {

    private bool grabbing = false;
    private Vector3 prevLoc;
    private Queue<Vector3> velQueue;

    public float multiplier = 1.5F;


	// Use this for initialization
	void Start () {

        prevLoc = gameObject.transform.position;
        velQueue = new Queue<Vector3>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void FixedUpdate()
    {
        //Debug.Log("Curr: " + lastVel);
        //Debug.Log(grabbing);
        Vector3 vel = new Vector3();
        Vector3 currentLoc = gameObject.transform.position;

        vel = (currentLoc - this.prevLoc) / Time.fixedDeltaTime;

        prevLoc = currentLoc;

        if (currentLoc.sqrMagnitude >= 2500) // more than 50 units away from origin, destroy
        {
            Destroy(gameObject);
            return;
        }

        //dont bother queing if we're not grabbing
        if(!grabbing)
        {
            if(velQueue.Count > 0)
                velQueue.Clear();
            return;
        }

        //Keep the last 3/20th of a second worth of velocities (15 for timescale = .01)
        if(velQueue.Count >= (.15) / Time.fixedDeltaTime)
        {
            velQueue.Dequeue();
        }
        velQueue.Enqueue(vel);         
        
    }

    public void OnRelease()
    {

        Vector3 lastVel = AverageVelocityQueue()*multiplier; //increase force 50%
        Debug.Log("Hand Release: velocity to " + lastVel);
        gameObject.GetComponent<Rigidbody>().velocity = lastVel;

        grabbing = false;

    }

    public void OnGrab()
    {
        Debug.Log("Hand grabbed");
        grabbing = true;
    }

    private Vector3 AverageVelocityQueue()
    {
        if (velQueue.Count <= 0) return new Vector3();
        return velQueue.Aggregate<Vector3>((x,y) => x + y) / velQueue.Count;
    }

}
