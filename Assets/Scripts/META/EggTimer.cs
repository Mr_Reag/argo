﻿using Meta;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EggTimer : MonoBehaviour {

    private bool rotating = false;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        Quaternion rot = transform.rotation;
        Vector3 angles = rot.eulerAngles;


        if (angles.y > 5 && !rotating)
            StartCoroutine(Rotate(5F));

	}

    private IEnumerator Rotate(float degs)
    {
        rotating = true;
        float time = 0;
        float initialY = transform.localEulerAngles.y;
        Vector3 newEuler;
        while (time < 1f)
        {
            newEuler = transform.localEulerAngles;
            newEuler.y = MathUtility.LerpAngleUnclamped(initialY, degs, time);
            transform.localEulerAngles = newEuler;
            time += Time.deltaTime * .5F;
            yield return null;
        }

        newEuler = transform.localEulerAngles;
        newEuler.y = degs;
        transform.localEulerAngles = newEuler;
        rotating = false;
    }
}

