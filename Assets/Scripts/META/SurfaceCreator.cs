﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Meta;
using Meta.Interop.Buttons;

public class SurfaceCreator : MonoBehaviour {

    private bool firstClick = false;
    private ButtonState lastState = ButtonState.ButtonRelease;
    private List<GameObject> activeMarkers = new List<GameObject>();


    [SerializeField]
    private GameObject markerPrefab;
    [SerializeField]
    private GameObject wallPrefab;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //Simple logic for determining if a press was short or long
    public void OnCameraEvent(MetaButton button)
    {
        if(button.Type == ButtonType.ButtonCamera)
        {
            if(button.State == ButtonState.ButtonShortPress)
            {
                lastState = ButtonState.ButtonShortPress;
            }
            else if (button.State == ButtonState.ButtonLongPress)
            {
                lastState = ButtonState.ButtonLongPress;
            }

            if(button.State == ButtonState.ButtonRelease && lastState != button.State)
            {
                if(lastState == ButtonState.ButtonShortPress)
                {
                    OnShortPress();
                    Debug.Log("Short press logged");
                }
                else
                {
                    OnLongPress();
                    Debug.Log("Long press logged");
                }
                lastState = button.State;
            }
        }
    }


    private void OnShortPress()
    {
        if (!firstClick)
        {
            firstClick = true;
            Invoke("DoublePressReset", 1F);
        }
        else
        {
            OnDoublePress();
        }
    }

    private void OnDoublePress()
    {
        if (activeMarkers.Count == 0) { 
            GameObject m1 = Instantiate(markerPrefab);
            Vector3 camPos = Camera.main.transform.position;
            m1.transform.position = camPos + new Vector3(.1F, 0, .3F);
            MeshRenderer rend = m1.GetComponent<MeshRenderer>();
            rend.material.color = Color.red;
            GameObject m2 = Instantiate(markerPrefab);
            m2.transform.position = camPos + new Vector3(-.1F, 0, .3F);
            //rend = m2.GetComponent<MeshRenderer>();
            //rend.material.color = Color.blue;
            GameObject m3 = Instantiate(markerPrefab);
            m3.transform.position = camPos + new Vector3(0, -.1F, .3F);

            activeMarkers.Add(m1);
            activeMarkers.Add(m2);
            activeMarkers.Add(m3);
        }
    }

    private void DoublePressReset()
    {
        firstClick = false;
    }

    private void OnLongPress()
    {
        if (activeMarkers.Count == 0)
        {
            Debug.Log("Long press, but no active markers");
            return;
        }

        GameObject m1 = activeMarkers.ToArray()[0];
        GameObject m2 = activeMarkers.ToArray()[1];
        GameObject m3 = activeMarkers.ToArray()[2];

        Vector3 v1 = m1.transform.position;
        Vector3 v2 = m2.transform.position;
        Vector3 v3 = m3.transform.position;

        Plane infpln = new Plane(v1, v2, v3);

        Vector3 normal = infpln.normal;

        GameObject plane = Instantiate(wallPrefab);
        Vector3 spnpnt = m1.transform.GetChild(0).position;


        plane.transform.position = spnpnt;

        plane.transform.rotation = Quaternion.LookRotation(normal);
        plane.transform.Rotate(90, 0, 0);

        activeMarkers.Clear();
        Destroy(m1);
        Destroy(m2);
        Destroy(m3);
    }
}
