﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Meta.HandInput;

namespace Meta
{
    /// <summary>
    /// Manipulation to rotate through average the quaternions of two hands grabbing around a central point.
    /// This version is experimental and tries to smooth the noise on the axis
    /// </summary>
    [AddComponentMenu("Meta/Manipulation/TwoHandRotateAxisInteraction")]
    public class TwoHandRotateAxisInteraction : TwoHandInteraction
    {

        public bool useXAxis = true;
        public bool useYAxis = true;
        public bool useZAxis = true;

        private Vector3 _priorDirection;

        private Vector3 scale;




        protected override void Engage()
        {
            _priorDirection = SecondGrabbingHand.transform.position - FirstGrabbingHand.transform.position;
            scale = Vector3.zero;
            if (useXAxis) scale += Vector3.right;
            if (useYAxis) scale += Vector3.up;
            if (useZAxis) scale += Vector3.forward;

            base.Engage();
        }

        protected override void Disengage()
        {
            base.Disengage();
        }

        /// <summary>
        /// Continually place gizmo between two hands and average the movement vectors of the two hands
        /// in order to apply to the final object quaternion around the gizmo center point.
        /// </summary>
        protected override void Manipulate()
        {
            Vector3 direction = SecondGrabbingHand.transform.position - FirstGrabbingHand.transform.position;

            Quaternion rotation = Quaternion.FromToRotation(_priorDirection, direction);
            Quaternion newRotation = rotation * TargetTransform.rotation;

            newRotation.eulerAngles = Vector3.Scale(newRotation.eulerAngles, scale);

            Rotate(newRotation);

            _priorDirection = direction;
        }
    }
}
