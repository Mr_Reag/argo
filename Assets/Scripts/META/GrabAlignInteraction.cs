﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Meta;


//Aligns the grabbed object to the supplied values at movespeed
public class GrabAlignInteraction : GrabInteraction
{
    //If any of these are set to negative, the values will not be changed!
    public float X=0;
    public float Y=0;
    public float Z=0;

    public float moveSpeed = 5F;

    protected override void Engage()
    {
        float x=X, y=Y, z=Z;
        if (X < 0) x = transform.rotation.eulerAngles.x;
        if (Y < 0) y = transform.rotation.eulerAngles.y;
        if (Z < 0) z = transform.rotation.eulerAngles.z;
        StartCoroutine(RotateToDefault(x,y,z));
        base.Engage();
    }


    IEnumerator RotateToDefault(float x, float y, float z)
    {
        var v = new Vector3(x, y, z);
        Debug.Log(y);
        while (Quaternion.Angle(transform.rotation, Quaternion.Euler(v)) > 1) //more than 1 degree difference between them
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(v), moveSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame(); //wait till end of frame
        }
        transform.rotation = Quaternion.Euler(v); //set it exact

        yield return null;
    }
}
