﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopulateMesh : MonoBehaviour {

    public GameObject prefab;
    public float scale;
    public float percent;



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void BeginPopulation()
    {
        if (GameObject.Find("LoadedReconstruction") != null)
            Populate();
        else
        {
            Debug.Log("Waiting");
            Invoke("BeginPopulation", 2F);

        }
    }
    
    private void Populate()
    {
        

        GameObject reconstruct = GameObject.Find("LoadedReconstruction");
        MeshFilter[] meshes = reconstruct.GetComponentsInChildren<MeshFilter>();
        foreach (MeshFilter mesh in meshes)
        {
            Dictionary<Vector3, Vector3> map = MeshUtilities.PickRandomVerticiesAndNormals(mesh.mesh, percent);
            MeshUtilities.PlacePrefabAtLocations(map, reconstruct, prefab,scale);
        }
    }

}
