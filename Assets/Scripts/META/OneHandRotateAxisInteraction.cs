﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Meta.HandInput;

namespace Meta
{

    public class OneHandRotateAxisInteraction : GrabInteraction
    {

        public enum LockType
        {
            XLOCK, YLOCK, ZLOCK, NONE
        };


        private Vector3 _priorDirection;
        private Transform center;


        public LockType axisLock = LockType.NONE;

        private HandFeature handFeature;

        public bool UseTargetTransformAsAxisCenter = false;


        protected override void Engage()
        {
            center = (UseTargetTransformAsAxisCenter) ? TargetTransform : transform;
            handFeature = GrabbingHands[0];
            _priorDirection = center.transform.position - handFeature.transform.position;

            if (axisLock == LockType.XLOCK) _priorDirection.x = 0;
            if (axisLock == LockType.YLOCK) _priorDirection.y = 0;
            if (axisLock == LockType.ZLOCK) _priorDirection.z = 0;

            base.Engage();
        }

        protected override void Disengage()
        {
            base.Disengage();
        }

        protected override void Manipulate()
        {
            Vector3 direction = center.transform.position - handFeature.transform.position;
            //debug set, x=0 -> rotate around x axis
            // y=0 -> rotate around y axis
            // z=0 -> rotate around z axis

            if (axisLock == LockType.XLOCK) direction.x = 0;
            if (axisLock == LockType.YLOCK) direction.y = 0;
            if (axisLock == LockType.ZLOCK) direction.z = 0;

            Quaternion rotation = Quaternion.FromToRotation(_priorDirection, direction);
            Quaternion newRotation = rotation * TargetTransform.rotation;
            //Quaternion newRotation = Quaternion.LookRotation(direction);


            Rotate(newRotation);

            _priorDirection = direction;
        }
    }
}
