﻿using Meta;
using Meta.HandInput;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

public class RecoilGrabInteraction : GrabInteraction {

    [SerializeField]
    private float maxPull = 1.0F;

    [SerializeField]
    private float snapSpeed = 1.0F;

    [SerializeField]
    private Transform origPosition;

    [SerializeField]
    private UnityEvent recoilStartTriggered;


    private bool lerping;
    private Vector3 origPos;
    private Quaternion origRot;

    protected override void Awake()
    {
        origPos = TargetTransform.position;

        base.Awake();
    }



    protected override void Update()
    {
        if(lerping)
        {
            if (origPosition != null) origPos = origPosition.position;
            TargetTransform.position = Vector3.Lerp(TargetTransform.position, origPos, Time.deltaTime * snapSpeed);
            if(Dist() <= .01)
            {
                TargetTransform.position = origPos;
                TargetTransform.rotation = origRot;
                lerping = false;
            }
        }

        base.Update();
    }


    protected override void Engage()
    {
        if (lerping) return;
        origRot = TargetTransform.rotation;
        origPos = TargetTransform.position;
        base.Engage();
    }


    protected override void Disengage()
    {
        if (lerping) return;
        lerping = true;
        if(Dist() >= maxPull/10) recoilStartTriggered.Invoke();
        base.Disengage();
    }

    protected override void Manipulate()
    {
        //Do sphere project
        if (GrabbingHands.Count < 1) return;
        Vector3 pos = GrabbingHands[0].Position;
        Vector3 dir = (pos - origPos).normalized;
        TargetTransform.rotation = Quaternion.LookRotation(TargetTransform.forward, dir);

        if (Dist() >= maxPull/10)
        {
            Vector3 mag = dir * (maxPull/10) * 1.05F;
            TargetTransform.position = origPos + mag;            
            return;
        }
        base.Manipulate();
    }




    protected override bool CanDisengage(Hand handProxy)
    {
        if (lerping) return false;
        return base.CanDisengage(handProxy);
    }

    protected override bool CanEngage(Hand handProxy)
    {
        if (lerping) return false;
        return base.CanEngage(handProxy);
    }

    private float Dist()
    {
        return (TargetTransform.position - origPos).magnitude;
    }
}
