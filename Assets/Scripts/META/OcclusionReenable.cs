﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Meta;
using Meta.Plugin;

public class OcclusionReenable : MonoBehaviour {


    public static bool occlusionActive = false;


    public static void EnableOcclulusion()
    {
        MetaCompositor comp = FindObjectOfType<MetaCompositor>();//gameObject.GetComponent<MetaCompositor>();
        if(comp != null)
        {
            comp.EnableHandOcclusion = true;
            occlusionActive = true;
        }
    }

    public static void DisableOcculusion()
    {
        MetaCompositor comp = FindObjectOfType<MetaCompositor>();//gameObject.GetComponent<MetaCompositor>();
        if (comp != null)
        {
            comp.EnableHandOcclusion = false;
            occlusionActive = false;
        }
    }
}
