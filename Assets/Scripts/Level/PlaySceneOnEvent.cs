﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySceneOnEvent : MonoBehaviour {

    public float timeToReset = 0f;


	public void PlayScene()
    {
        if (!ResetController.IsScenePlaying())
        {
            ResetController.PlayScene();
            if (timeToReset > 0) StartCoroutine(ResetOnTime());
        }
    }


    public void ResetScene()
    {
        if(ResetController.IsScenePlaying() && !ResetController.IsSceneWon())
            ResetController.ResetScene();
    }

    IEnumerator ResetOnTime()
    {
        int counter = 0;
        while(ResetController.IsScenePlaying())
        {
            yield return new WaitForSecondsRealtime(1F);
            counter++;
            if(counter > timeToReset)
            {
                ResetScene();
            }
        }
        yield return null;
    }
}
