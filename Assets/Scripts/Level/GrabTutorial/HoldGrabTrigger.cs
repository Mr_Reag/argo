﻿using Meta;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoldGrabTrigger : Interaction {

    private SceneTrigger control;

    private void Start()
    {
        control = transform.root.GetComponentInChildren<SceneTrigger>();
    }

    protected override void Engage()
    {
        StartCoroutine(TimerCoroutine());
    }


    protected override void Disengage()
    {
        StopCoroutine(TimerCoroutine());
    }


    IEnumerator TimerCoroutine()
    {
        yield return new WaitForSecondsRealtime(5);
        control.LoadSceneAsync();
        yield return null;
    }

    protected override void Manipulate()
    {
        //
    }
}
