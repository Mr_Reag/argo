﻿using Meta;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoHandHoldGrabTrigger : TwoHandGrabInteraction {

    private SceneTrigger control;

    [SerializeField]
    private float timeUntilTrigger = 2F;

    private bool grabbing = false;

    private void Start()
    {
        control = transform.root.GetComponentInChildren<SceneTrigger>();
    }

    protected override void Engage()
    {
        if (!grabbing)
        {
            grabbing = true;
            StartCoroutine(TimerCoroutine());
        }
    }


    protected override void Disengage()
    {
        grabbing = false;
        StopAllCoroutines();
    }


    IEnumerator TimerCoroutine()
    {
        yield return new WaitForSecondsRealtime(timeUntilTrigger);
        if (grabbing)
        {    
            control.LoadSceneAsync();
        }
        yield return null;
    }
}
