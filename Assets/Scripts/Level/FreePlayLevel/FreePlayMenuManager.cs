﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class FreePlayMenuManager : MonoBehaviour {


    private static FreePlayMenuManager singleton;

    [SerializeField]
    private GameObject spawnButton;
    private static GameObject button;

    [SerializeField]
    private GameObject spawnMenu;
    private static GameObject menu;
    


    private readonly object l = new object();


    private static bool buttonShown;


    //Create a singleton of this manager, and set up the scene for use
	void Start () {
        lock (l)
        {
            if (singleton == null)
                singleton = this;
            else
                Destroy(this);
        }
        spawnButton.SetActive(true);
        spawnMenu.SetActive(false);
        button = spawnButton;
        menu = spawnMenu;

        buttonShown = true;
            
	}
	

    public void ToggleMenus()
    {
        StartCoroutine(ToggleCoroutine());
    }

    public static void StaticToggleMenus()
    {
        if(buttonShown)
        {
            button.SetActive(false);
            menu.SetActive(true);
            SoundManager.PlayAudioClip("menu-select");
        }
        else
        {
            button.SetActive(true);
            menu.SetActive(false);
            SoundManager.PlayAudioClip("menu-back");
        }

        buttonShown = !buttonShown;
    }

    private static readonly object routineLock = new object();
    static IEnumerator ToggleCoroutine()
    {
        //Attempt to aquire the lock. If we can, toggle the menu. Otherwise break
        if (!Monitor.TryEnter(routineLock)) yield break; //exit coroutine
        else
            try
        {
            if (buttonShown)
            {
                SoundManager.PlayAudioClip("menu-select");
                yield return new WaitForSecondsRealtime(.12F);
                button.SetActive(false);
                menu.SetActive(true);

            }
            else
            {
                SoundManager.PlayAudioClip("menu-back");
                yield return new WaitForSecondsRealtime(.14F);
                button.SetActive(true);
                menu.SetActive(false);
            }

            buttonShown = !buttonShown;
            yield return new WaitForSecondsRealtime(.5f); //Cooldown
        } finally
        {        
            Monitor.Exit(routineLock);
        }      
        yield return null;
    }


}
