﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetOnLost : MonoBehaviour {

    int cooldown = 0;
	
	// Update is called once per frame
	void Update () {
        float distsqr = (Camera.main.transform.position - transform.position).sqrMagnitude;

        if(distsqr > 25F && cooldown == 0)
        {
            GetComponent<TorusScript>().OnResetLocation();
            cooldown = 100;
        }
        if (cooldown > 0) cooldown--;
	}
}
