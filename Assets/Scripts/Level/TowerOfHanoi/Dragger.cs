﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dragger : MonoBehaviour {

    public bool isTouching;
    public bool isEnabled;

    public float moveSpeed = 5F;

    private void Start()
    {
        isTouching = false;
        //GetComponent<Rigidbody>().isKinematic = true;
    }

    private void OnMouseDrag()
    {
        if (isEnabled && GetComponent<TorusScript>().isEnabled)
        {
            isTouching = true;
            if (GetComponent<Rigidbody>() != null)
            {
                //GetComponent<Rigidbody>().isKinematic = true;
            }
            Vector3 touchPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, transform.position.z - Camera.main.transform.position.z);
            Vector3 objPosition = Camera.main.ScreenToWorldPoint(touchPosition);
            objPosition.z = 0.6f;
            transform.position = objPosition;

            StartCoroutine(RotateToDefault(0, 0, 0));
        }
    }

    private void OnMouseUp()
    {
        isTouching = false;
        if (GetComponent<Rigidbody>() != null)
        {
           GetComponent<Rigidbody>().isKinematic = false;
        }
    }

    IEnumerator RotateToDefault(float x, float y, float z)
    {
        var v = new Vector3(x, y, z);
        //Debug.Log(y);
        while (Quaternion.Angle(transform.rotation, Quaternion.Euler(v)) > 1) //more than 1 degree difference between them
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(v), moveSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame(); //wait till end of frame
        }
        transform.rotation = Quaternion.Euler(v); //set it exact

        yield return null;
    }
}
