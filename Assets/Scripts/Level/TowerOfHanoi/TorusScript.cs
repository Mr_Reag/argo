﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorusScript : MonoBehaviour {

    public int TorusIndex;
    public int CurrentTowerIndex;
    public float lastXPosition;
    public float lastYPosition;
    public float lastZPosition;
    public TowerIndexScript[] Towers;
    public TorusScript[] OtherToruses;

    public bool isEnabled;

    private void Start()
    {
        //OnResetLocation();
        if (TorusIndex == 1)
        {
            disableOtherTorusInteractablesOnTower(0);
            gameObject.GetComponent<Dragger>().enabled = true;
            gameObject.GetComponent<GrabAlignInteraction>().enabled = true;
        }

        isEnabled = true;
    }

    public void OnResetLocation()
    {
        this.transform.SetPositionAndRotation(new Vector3(lastXPosition, lastYPosition, lastZPosition), new Quaternion());
        StartCoroutine(enable());
    }

    private void OnTriggerEnter(Collider other)
    {
        string tag = other.gameObject.tag;
        if (!(tag.Equals("Tower") || !tag.Equals("Reset")) || !isEnabled)
        {
            return;
        }

        if (tag.Equals("Resets"))
        {
            OnResetLocation();
            return;
        }

        if (tag.Equals("Tower"))
        {
            TowerIndexScript tower = other.gameObject.GetComponent<TowerIndexScript>();
            if (tower.TowerIndex == CurrentTowerIndex)
            {
                return;
            }
            
            if (tower.TopTorusIndex < this.TorusIndex) //check if the grabbed torus is larger than the top torus on the tower (illegal)
            {
                isEnabled = false;
                OnResetLocation();
                return;
            }
            else //the tower either has no toruses or it's top torus is larger than the grabbed torus (legal)
            {
                changeTower(tower);
            }
        }
    }

    private void changeTower(TowerIndexScript newTower)
    {
        int previousTowerIndex = this.CurrentTowerIndex;
        this.CurrentTowerIndex = newTower.TowerIndex;
        newTower.TopTorusIndex = this.TorusIndex;

        int previousTowerTopTorusIndex = findPreviousTowerTopTorusIndex(previousTowerIndex);
        Towers[previousTowerIndex].TopTorusIndex = previousTowerTopTorusIndex;

        enableTorusInteractables(previousTowerTopTorusIndex); //frees up the top torus on the previous tower
        disableOtherTorusInteractablesOnTower(CurrentTowerIndex); //disables any other toruses on the new tower

        updateLastPositions();
    }

    private void updateLastPositions()
    {
        switch (this.CurrentTowerIndex)
        {
            case 0:
                lastXPosition = -0.4f;
                break;

            case 1:
                lastXPosition = 0f;
                break;

            case 2:
                lastXPosition = 0.4f;
                break;

            default:
                break;
        }

        lastYPosition = 0f;
        lastZPosition = 0.6f;
    }

    private void disableOtherTorusInteractablesOnTower(int currentTowerIndex)
    {
        foreach (TorusScript otherTorus in OtherToruses)
        {
            if (otherTorus.CurrentTowerIndex == currentTowerIndex)
            {
                int otherTorusIndex = otherTorus.TorusIndex;
                disableTorusInteractables(otherTorusIndex);
            }
        }
    }

    private int findPreviousTowerTopTorusIndex(int previousTowerIndex)
    {
        int result = 10;

        foreach (TorusScript otherTorus in OtherToruses)
        {
            if (otherTorus.CurrentTowerIndex == previousTowerIndex)
            {
                if (otherTorus.TorusIndex < result)
                {
                    result = otherTorus.TorusIndex;
                }
            }
        }

        return result == 0 ? 10 : result;
    }

    private void enableTorusInteractables(int torusIndex)
    {
        foreach (TorusScript torus in OtherToruses)
        {
            if (torus.TorusIndex == torusIndex)
            {
                torus.gameObject.GetComponent<Dragger>().isEnabled = true;
                torus.gameObject.GetComponent<GrabAlignInteraction>().enabled = true;
            }
        }
    }

    private void disableTorusInteractables(int torusIndex)
    {
        foreach (TorusScript torus in OtherToruses)
        {
            if (torus.TorusIndex == torusIndex)
            {
                torus.gameObject.GetComponent<Dragger>().isEnabled = false;
                torus.gameObject.GetComponent<GrabAlignInteraction>().enabled = false;
            }
        }
    }

    private IEnumerator enable()
    {
        yield return new WaitForSeconds(1f);
        isEnabled = true;
    }
}
