﻿using Meta.HandInput;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;
using Meta;

public class SceneController : MonoBehaviour {


    [SerializeField]
    private bool useFog = false;

    [SerializeField]
    private bool useOcclusion = false;

    private static bool _useOcc = false;

    public static string sceneName;

    private static bool started = false;

    private List<GameObject> disabledObjects;
    private static FogFader staticFog;

    private static object lk = new object();

    private static readonly string[] whitelist =
    {
        "Meta",
        "Reconstruction",
        "ScannerController",
        "instantiator",
        "Reconstruction"
    };

    private void Awake()
    {
        _useOcc = useOcclusion;

        GameObject[] roots = SceneManager.GetActiveScene().GetRootGameObjects();
        disabledObjects = roots.Where(x => (x.tag == "Hideable") && x.activeInHierarchy).ToList(); //list of all active objects in the scene that are not meta objects
        disabledObjects.ForEach(x => x.SetActive(false)); //disable all of them

        staticFog = GetComponent<FogFader>();

        if (!useFog) staticFog = null;
        if (staticFog != null) staticFog.RestoreFog();
    }


    public void OnSlamMappingComplete()
    {
        if(staticFog != null) OcclusionReenable.DisableOcculusion(); //Disable the Occlusion for the main menu
        disabledObjects.ForEach(x => x.SetActive(true));
        if (staticFog != null) staticFog.FadeFogOut();
    }

    public void LoadSceneAsync(string scene)
    {
        sceneName = scene;
        LoadSceneAsync();
    }

    private void LoadSceneAsync()
    {
        lock (lk)
        {
            if (!started)
            {
                started = true;
                StartCoroutine(ScenePreloader(sceneName));
                // OcclusionReenable.EnableOcclulusion(); //reenable occlusion for subsequent scenes
            }
        }      
    }

    static IEnumerator ScenePreloader(string sceneName)
    {
        //Debug.Log("Scene Preloader active for " + sceneName);
        AsyncOperation load = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        load.allowSceneActivation = false; //stop the scene from loading immidiatly

        if (staticFog != null) staticFog.FadeFogIn();

        while (!load.isDone)
        {
            yield return new WaitForSecondsRealtime(.1F);
            if (load.progress < .9F)
            {
               //Kept for debugging purposes
               // Debug.Log("Progress is " + load.progress + " for scene " + sceneName);

            }
            else
            {
                for (int i = 0; i < SceneManager.sceneCount; i++)
                {
                    Scene s = SceneManager.GetSceneAt(i);
                    if (s.Equals(SceneManager.GetActiveScene()))
                    {
                        continue;
                    }
                    DisableByTag(s);
                }
                if (staticFog != null)
                {
                    yield return new WaitUntil(() => staticFog.doneFading);
                    yield return new WaitForSecondsRealtime(.1F);
                }
                load.allowSceneActivation = true; //load next scene, then clean up the next one
                DeleteActiveScene();
            }
        }

        

        //Debug.Log("Merging Scenes");
        MergeMetaScenes(); //merge our scenes so we only have one active scene at a time    

        if (staticFog != null) staticFog.FadeFogOut();
        if (_useOcc && !OcclusionReenable.occlusionActive) OcclusionReenable.EnableOcclulusion();
        started = false;
        ResetController.SetupResetables(); //Setup the resettable items for the next scene
        yield return null;
    }



    private static void MergeMetaScenes()
    {
        Scene active = SceneManager.GetActiveScene();
        List<Scene> sceneList = new List<Scene>();
        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            Scene s = SceneManager.GetSceneAt(i);
            //Debug.Log(s.name);
            if (s.Equals(active)) continue;
            else sceneList.Add(s);
        }
        //Debug.Log(sceneList.Select(x => x.name).Aggregate((i, j) => i + ", " + j));
        sceneList.ForEach(x => SceneManager.MergeScenes(x, active));
    }


    private static void DeleteActiveScene()
    {
        GameObject[] roots = SceneManager.GetActiveScene().GetRootGameObjects();
        //Debug.Log(roots.Select(x => x.name).Aggregate((i, j) => i + ", " + j));
        roots.Where(x => ! (whitelist.Any(wl => x.name.Contains(wl))) )       
            .ToList().ForEach(x =>           
            {
                MetaDelete.DeleteMetaObject(x);
            }); //destroy all objects that are not on the white list

    }


    private static void DisableByTag()
    {
        SestroyByTag(SceneManager.GetActiveScene(), "DisableOnLoad");
    }

    private static void DisableByTag(Scene scene)
    {
        SestroyByTag(scene, "DisableOnLoad");
    }

    //Change to destroy when done testing
    private static void SestroyByTag(Scene scene,string tag)
    {
        GameObject[] roots = scene.GetRootGameObjects();
        roots.Where(x => x.tag == tag).ToList().ForEach(x =>
        {
            DestroyImmediate(x);
        });
    }
}
