﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Note that this is a singleton class
public class SceneControllerManager : MonoBehaviour {

    public static SceneControllerManager instance = null;
    private static SceneController  sceneController;
    private static ResetController resetController;

    private static bool isSetup = false;

    private void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(gameObject);

        sceneController = GetComponentInChildren<SceneController>();
        if (sceneController == null) sceneController = gameObject.AddComponent(typeof (SceneController)) as SceneController;

        resetController = GetComponentInChildren<ResetController>();
        if (resetController == null) resetController = gameObject.AddComponent(typeof(ResetController)) as ResetController;

        isSetup = true;
    }

    public static SceneController GetSceneController()
    {
        if(sceneController != null)
            return sceneController;
        Debug.LogError("SceneControllerManager not set up!");
        return null;
    }

    public static bool IsSceneControllerSetup()
    {
        return sceneController != null;
    }

    public static bool IsManagerSetup()
    {
        return isSetup;
    }
}
