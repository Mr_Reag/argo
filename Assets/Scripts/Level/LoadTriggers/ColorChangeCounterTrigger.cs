﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;



/**
 * 
 * 
 * Changes the color of the selected sprites when something enteres the triggercollider. changes it back when it exits
 * 
 * 
 */
 


public class ColorChangeCounterTrigger : MonoBehaviour {

    [SerializeField]
    private TriggerLoadOnCount trigger;

    [SerializeField]
    private SpriteRenderer[] spriteArr;

    [SerializeField]
    private string prefabName;

    [SerializeField]
    private Color colorToApply = new Color(0,1,0,1);

    [SerializeField]
    private bool ignoreKinematic = true;

    private Dictionary<SpriteRenderer,Color> oldColor = new Dictionary<SpriteRenderer, Color>();
    private bool activated;
    private GameObject collobj;

    private List<SpriteRenderer> sprites;

    private void Awake()
    {
        sprites = spriteArr.ToList();
    }

    void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Triggered by " + other.name);
        if(!activated && (prefabName == "" || other.gameObject.name.Contains(prefabName)))
        {
            var rb = other.gameObject.GetComponent<Rigidbody>();
            if (rb == null || (ignoreKinematic && rb.isKinematic)) return;
            sprites.ForEach(x => 
            {
                oldColor.Add(x, x.color);
                x.color = colorToApply;
            });
            trigger.IncreaseCounter();
            activated = true;
            collobj = other.gameObject;
            //Debug.Log("Triggered");
        }
    }


    void OnTriggerExit(Collider other)
    {
        if(!activated || other.gameObject != collobj) return;
        if (prefabName == "" || other.gameObject == collobj)
        {
            sprites.ForEach(x => x.color = oldColor[x]);
            oldColor.Clear();
            trigger.DecreaseCount();
            activated = false;  
            collobj = null;
        }
    }

    public void CallColorTriggerEvent()
    {
        sprites.ForEach(x =>
        {
            x.color = colorToApply;
        });
        trigger.IncreaseCounter();
    }

}
