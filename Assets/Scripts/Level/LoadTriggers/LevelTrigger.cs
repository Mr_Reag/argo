﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelTrigger : MonoBehaviour
{

    [SerializeField]
    private TriggerLoadOnCount countToTrigger;

    [SerializeField]
    private GameObject[] triggerObjects;

    private void OnTriggerEnter(Collider other)
    {
        if (ResetController.IsSceneWon()) return;
        if (other.gameObject == null) return;

        for (int i = 0; i < triggerObjects.Length; i++)
        {
            var x = triggerObjects[i];
            if (x == null) continue;
            if (other.gameObject.Equals(x))
            {
                countToTrigger.IncreaseCounter();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (ResetController.IsSceneWon()) return;
        if (other.gameObject == null) return;

        for (int i = 0; i < triggerObjects.Length; i++)
        {
            var o = triggerObjects[i];
            if (o == null) continue;
            if (other.gameObject.Equals(o))
            {
                countToTrigger.DecreaseCount();         
            }
        }
    }
}
