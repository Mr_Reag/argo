﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Meta.HandInput;
using UnityEngine.SceneManagement;
using System.Linq;
using System.Threading;

public class SceneTrigger : MonoBehaviour {

    [SerializeField]
    private bool useHandTrigger = false;

    public string sceneName;

    public void OnHandFeatureEnterEvent(HandFeature hand)
    {
        if (!useHandTrigger) return;
        SoundManager.PlayAudioClip("menu-select");
        LoadSceneAsync();
    }

    protected virtual void EndScene()
    {
        ResetController.WinScene();
        LoadSceneAsync();
    }

    public void LoadSceneAsync()
    {
        SceneControllerManager.GetSceneController().LoadSceneAsync(sceneName);
    }
}
