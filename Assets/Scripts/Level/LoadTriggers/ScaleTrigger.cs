﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ScaleTrigger : MonoBehaviour {

    [SerializeField]
    private float scaleToTrigger = 1F;

    [SerializeField]
    private TriggerLoadOnCount trigger;

    [SerializeField]
    private SpriteRenderer[] sprites;

    [SerializeField]
    private Color colorToApply = new Color(0,1,0,1);

    private void Awake()
    {
        StartCoroutine(WaitForScale(transform));
    }


    IEnumerator WaitForScale(Transform trs)
    {
        yield return new WaitUntil(() => trs.localScale.sqrMagnitude > scaleToTrigger * scaleToTrigger);
        sprites.ToList().ForEach(x => x.color = colorToApply);
        trigger.IncreaseCounter();        
        yield return null;
    }
}
