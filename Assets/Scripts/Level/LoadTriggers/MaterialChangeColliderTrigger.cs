﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MaterialChangeColliderTrigger : MonoBehaviour {

    [SerializeField]
    private Material changeToOnCollide;

    [SerializeField]
    private TriggerLoadOnCount countToTrigger;

    [SerializeField]
    private GameObject[] triggerObjects;

    private Dictionary<MeshRenderer, Material> dict = new Dictionary<MeshRenderer, Material>();

    private void OnTriggerEnter(Collider other)
    {
        if (ResetController.IsSceneWon()) return;
        if (other.gameObject == null) return;

        for(int i = 0; i < triggerObjects.Length;i++)
        {
            var x = triggerObjects[i];
            if (x == null) continue;
            if (other.gameObject.Equals(x))
            {
                var meshRend = GetComponentsInChildren<MeshRenderer>();
                meshRend.ToList().ForEach(y =>
                {
                    
                    dict.Add(y, y.material);
                    y.material = changeToOnCollide;
                });
                countToTrigger.IncreaseCounter();
                return;
            }
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (ResetController.IsSceneWon()) return;
        if (other.gameObject == null) return;

        for (int i = 0; i < triggerObjects.Length; i++)
        {
            var o = triggerObjects[i];
            if (o == null) continue;
            if (other.gameObject.Equals(o))
            {
                var meshRend = GetComponentsInChildren<MeshRenderer>();
                meshRend.ToList().ForEach(x =>
                {
                    x.material = dict[x];
                });
                countToTrigger.DecreaseCount();
                dict.Clear();
            }
        }
    }
}
