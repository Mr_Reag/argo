﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerLoadOnCount : SceneTrigger {

    [SerializeField]
    private int countToTrigger = 1;
    private int count;

    [SerializeField]
    private float timeToWait = 3.0F;

    [SerializeField]
    private UnityEvent levelWonEvent;


    public void IncreaseCounter()
    {
        count++;
        if (count >= countToTrigger) EndScene();
    }

    public void DecreaseCount()
    {
        if (count > 0) count--;
    }

    public void ResetCounter()
    {
        count = 0;
    }


    protected override void EndScene()
    {
        StartCoroutine(LoadAfterDelay(timeToWait));
    }

    IEnumerator LoadAfterDelay(float delay)
    {
        yield return new WaitForEndOfFrame(); //Wait for end of frame to allow for ending behaviors to clean up (ex: giving gravity to objects)
        levelWonEvent.Invoke();
        ResetController.WinScene();
        yield return new WaitForSeconds(delay);
        LoadSceneAsync();
        yield return null;
    }
}
