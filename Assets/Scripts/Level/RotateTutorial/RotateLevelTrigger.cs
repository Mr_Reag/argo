﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateLevelTrigger : MonoBehaviour {

    private SceneTrigger control;

    [SerializeField]
    public float timeUntilTrigger = 2F;

    private bool grabbing = false;

    private void Start()
    {
        control = transform.root.GetComponentInChildren<SceneTrigger>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (!grabbing)
        {
            grabbing = true;
            StartCoroutine(TimerCoroutine());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        grabbing = false;
        StopAllCoroutines();
    }

    IEnumerator TimerCoroutine()
    {
        yield return new WaitForSecondsRealtime(timeUntilTrigger);
        if (grabbing)
            control.LoadSceneAsync();
        yield return null;
    }
}
