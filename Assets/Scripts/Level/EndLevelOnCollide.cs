﻿using Meta;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLevelOnCollide : MonoBehaviour {

    private SceneTrigger control;

    [SerializeField]
    private string triggerName;

	// Use this for initialization
	void Start () {
        control = transform.root.GetComponentInChildren<SceneTrigger>();
	}


    private void OnTriggerStay(Collider other)
    {
        GameObject master = other.transform.root.gameObject; //get the highest level parent

        //See if the object is in our whitelist. Dont delete it if it is
        if (!master.name.Equals(triggerName))
        {
            return;
        }


        GrabInteraction grabbable = master.GetComponentInChildren<GrabInteraction>(); //can we grab this object?

        if (grabbable != null && grabbable.State == InteractionState.Off) //if we can delete this and its not being grabbed
        {
            control.LoadSceneAsync();
        }
    }
}
