﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;
using Meta.HandInput;

public class ResetController : MonoBehaviour
{

    private List<GameObject> resetables;
    private List<Vector3> locationList;
    public static ResetController Instance { get; private set; }

    public enum STATE
    {
        PLAYING,
        PAUSED,
        WIN
    }

    public static STATE SceneState { get; private set; }

    //Lock for singleton
    private static readonly object Lock = new object();
	//We init on a controlled resource. performance is of less importance here
    //Note that this class is a singleton
	void Awake () {
        lock (Lock)
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this); //remove this game object from the scene
            }
            else
            {
                Instance = this;
                SetupResetables();
            }
        }
    }

    public static void SetupResetables()
    {      
        if (Instance == null) return;
        SceneState = STATE.PAUSED;
        Instance.resetables = SceneManager.GetActiveScene().GetRootGameObjects().Where(x => x.tag.Equals("Resets")).ToList();
        Instance.resetables.ForEach(x =>
        {
            var rb = x.GetComponentInChildren<Rigidbody>();
            if (rb == null) rb = x.AddComponent<Rigidbody>();
            rb.useGravity = false;
            rb.isKinematic = true;
        });
        Instance.locationList = new List<Vector3>();
        for (int i = 0; i < Instance.resetables.Count; i++)
        {
            Instance.locationList.Add(Instance.resetables[i].transform.position);
        }
    }
	
	public static void ResetScene()
    {
        if (Instance == null) return;
        if (IsSceneWon()) return;
        for (int i = 0; i < Instance.locationList.Count; i++)
        {
            Instance.resetables[i].transform.position = Instance.locationList[i];
            var rb = Instance.resetables[i].GetComponentInChildren<Rigidbody>();
            rb.useGravity = false;
            rb.isKinematic = true;
        }
        SceneState = STATE.PAUSED;
        SoundManager.PlayAudioClip("Error");
    }

    public static void PlayScene()
    {
        if (Instance == null) return;
        if (SceneState == STATE.PLAYING) return;
        if (IsSceneWon()) return;
        SceneState = STATE.PLAYING;
        Instance.resetables.ForEach(x =>
        {
            var rb = x.GetComponentInChildren<Rigidbody>();
            rb.useGravity = true;
            rb.isKinematic = false;
        });
    }

    public static void PauseScene()
    {
        if (Instance == null) return;
        if (SceneState == STATE.PAUSED) return;
        if (IsSceneWon()) return;
        SceneState = STATE.PAUSED;
        Instance.resetables.ForEach(x =>
        {
            var rb = x.GetComponentInChildren<Rigidbody>();
            rb.useGravity = false;
            rb.isKinematic = true;
        });
    }

    public static void WinScene()
    {
        SoundManager.PlayAudioClip("Fanfare");
        SceneState = STATE.WIN;
    }


    public static bool IsScenePlaying()
    {
        return SceneState == STATE.PLAYING;
    }

    public static bool IsSceneWon()
    {
        return SceneState == STATE.WIN;
    }
}
