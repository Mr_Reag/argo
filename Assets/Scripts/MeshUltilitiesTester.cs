﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MeshUltilitiesTester : MonoBehaviour {

    public GameObject prefab;

    // Use this for initialization
    void Start()
    {

        Mesh mesh = gameObject.GetComponent<MeshFilter>().mesh;

        Dictionary<Vector3, Vector3> map = MeshUtilities.PickRandomVerticiesAndNormals(mesh, 50);

        //gameObject.GetComponent<MeshFilter>().mesh = mesh;
        MeshUtilities.PlacePrefabAtLocations(map,gameObject,prefab);
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
