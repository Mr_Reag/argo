﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateGravity : MonoBehaviour
{

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void UpdateSpawnGravity(bool gravity)
    {
        SpawnObject.gravity = gravity;
        PlaySoundeffect sfx = gameObject.transform.root.GetComponentInChildren<PlaySoundeffect>();
        if (sfx != null) sfx.PlaySound();
    }
}
