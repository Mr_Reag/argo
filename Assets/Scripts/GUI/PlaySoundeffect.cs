﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundeffect : MonoBehaviour {

    public AudioSource source;
    public Meta.Audio.AudioRandomizer randomizer;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PlaySound()
    {
        randomizer.Play(source);
    }
}
