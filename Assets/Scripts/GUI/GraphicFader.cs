﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GraphicFader : MonoBehaviour {

    [SerializeField]
    private Graphic graphicObject;

    [SerializeField]
    private bool fadeInOnLoad = false;

    [SerializeField]
    private bool startVisible = false;

    public float delayBeforeFade = 3F;

	//fades in the help after 5 sconds
	void Start () {

       // Debug.Log("Graphics fader loaded");

        if (graphicObject == null) graphicObject = GetComponent<Image>();

        if(startVisible) graphicObject.CrossFadeAlpha(1, 0F, true);
        else graphicObject.CrossFadeAlpha(0, 0F, true);

        if (fadeInOnLoad)
        {
            FadeInAfterDelay();
        }    
	}

    public void FadeInAfterDelay()
    {
       // Debug.Log("fade in delay called");
        Invoke("FadeIn", delayBeforeFade);
    }
    public void FadeOutAfterDelay()
    {
        Invoke("FadeOut", delayBeforeFade);
    }

    public void FadeIn()
    {
       // Debug.Log("starting fade in");
        graphicObject.CrossFadeAlpha(1, 2F, true);
    }
    public void FadeOut()
    {
        graphicObject.CrossFadeAlpha(0, 2F, true);
    }
}
