﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Meta.HandInput.SwipeInteraction;

public class UpdateScaleSwipeInteraction : MonoBehaviour
{


    public SwipeIntensityInteraction swipe;


    // Update is called once per frame
    void Update()
    {
        if (swipe.SwipeOn)
        {
            float mag = swipe.Intensity + 1.001F;
            SpawnObject.scale = mag;
        }
    }
}
