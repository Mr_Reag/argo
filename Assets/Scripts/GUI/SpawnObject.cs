﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Meta;

public class SpawnObject : MonoBehaviour {

    public GameObject toSpawn;
    public GameObject spawnLocation;
    public static float scale = 1F;
    public static bool gravity = true;
    private bool cooldown = false;
    


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClick()
    {
        Debug.Log("Logged Click");

        if (cooldown) return;

        Vector3 pos = (spawnLocation != null) ? spawnLocation.transform.position  : gameObject.transform.position;
        Quaternion qu = (spawnLocation != null) ? spawnLocation.transform.rotation : gameObject.transform.rotation;
        GameObject spn = Instantiate(toSpawn,pos, qu);
        spn.transform.localScale*=scale;
        Rigidbody rb = spn.GetComponent<Rigidbody>();
        if(rb != null)
        {
            rb.useGravity = gravity;
            if (!gravity)
            {
                if(!spn.GetComponentInChildren<TurnTableSwipeInteraction>()) Destroy(rb); //destroy the rigid body
            }
                
        }

        cooldown = true;
        Invoke("ResetCooldown", 2F);

        PlaySoundeffect sfx = gameObject.transform.root.GetComponentInChildren<PlaySoundeffect>();
        if (sfx != null) sfx.PlaySound();
        
    }
    void ResetCooldown()
    {
        cooldown = false;
    }
}
