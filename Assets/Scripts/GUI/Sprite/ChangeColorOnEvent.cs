﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


/**
 * 
 * 
 * Changes the color o the selected sprites when an event is called. This differs from the ColorChangeCounterTrigger as all colors are updated on the event, rather that just the one that triggered it.
 * 
 * 
 */ 


public class ChangeColorOnEvent : MonoBehaviour {

    [SerializeField]
    private SpriteRenderer[] spriteArr;

    [SerializeField]
    private Color colorToApply = new Color(0, 1, 0, 1);

    private Dictionary<SpriteRenderer, Color> oldColor = new Dictionary<SpriteRenderer, Color>();

    private List<SpriteRenderer> sprites;

    private bool changed = false;

    private void Awake()
    {   
        sprites = spriteArr.ToList();
    }

    public void ChangeColor()
    {
        if (changed) return;
        sprites.ForEach(x =>
        {
            oldColor.Add(x, x.color);
            x.color = colorToApply;
        });
        changed = true;
    }

    public void ChangeColorBack()
    {
        if (!changed) return;
        sprites.ForEach(x => x.color = oldColor[x]);
        oldColor.Clear();
        changed = false;
    }
}
