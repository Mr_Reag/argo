﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class WebcamStreamer : MonoBehaviour {



    public RawImage rawimage;
    public int webcamIndex = 0;
    private int currentCam = 0;

    //private static string delimiter = ", ";

    private void Awake()
    {
        //Debug.Log(WebCamTexture.devices.Select(x => x.name).ToList().Aggregate((i, j) => i + delimiter + j));
    }

    void Start()
    {
        UpdateCamera();
    }
	
	// Update is called once per frame
	void Update () {
        if(webcamIndex >= WebCamTexture.devices.Count())
        {
            Debug.Log("Invalid webcam choice");
            Destroy(gameObject.transform.root);
            return;
        }
		if(webcamIndex != currentCam)
        {         
            UpdateCamera();
        }
	}


    void UpdateCamera()
    {
        string deviceName = WebCamTexture.devices[webcamIndex].name;
        WebCamTexture webcamTexture = new WebCamTexture(deviceName);
        rawimage.texture = webcamTexture;
        rawimage.material.mainTexture = webcamTexture;
        webcamTexture.Play();
        currentCam = webcamIndex;
    }
}
