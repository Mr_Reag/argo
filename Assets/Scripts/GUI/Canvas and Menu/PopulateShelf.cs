﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopulateShelf : MonoBehaviour {


    public GameObject[] prefabList;
    public GameObject grabbablePrefab;


    private GameObject[] spawnedGrabbables;


    // Use this for initialization
    void Start () {
        CreateGrabPrefab.grabbablePrefab = grabbablePrefab;
        BuildShelves();
	}
	
	public void BuildShelves()
    {
        //Find objects to spawn
        int spawncount = prefabList.Length;
        if (spawncount < 1) return;

        //Figure out if we need to scale up our canvases
        int scaleSteps = 0;
        if (spawncount > 12)
        {
            scaleSteps = (spawncount - 12) / 6 + 1;
        }

        //Set our scales
        if(scaleSteps > 0)
        {
            GameObject handle = transform.root.Find("HandleManager").gameObject;
            handle.transform.localScale += (scaleSteps / 2.0F) * Vector3.right; //scale handles by .5 per step

            RectTransform canvas = gameObject.GetComponent <RectTransform> ();
            canvas.sizeDelta = new Vector2(200*scaleSteps + 400, 300); //locked width. 
        }

        foreach (GameObject obj in prefabList)
        {
            GameObject point = Instantiate(new GameObject(), transform);
            point.AddComponent<RectTransform>();
            CreateGrabPrefab.CreateGrabbablePrefab(point.transform, obj, Vector3.one);


        }

    }
}
