﻿using Meta;
using Meta.HandInput;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


//Interaction for creating a new instance of a prefab when you grab this object
public class CloneGrabInteraction : Interaction {


    public GameObject spawnPrefab;
    public Camera eventCam;

    private Vector3 _localOffset;
    private GameObject lastInstant;
    //private GrabInteraction lastGrab;
    private List<Interaction> interactionList;
    private Hand grabHand;



    protected override void Disengage()
    {
        //if (lastGrab != null) return;
        //lastGrab.OnGrabDisengaged(grabHand);

        if (interactionList == null) return; //If we havent grabbed yet
        interactionList.ForEach(x => {if (x) x.OnGrabDisengaged(grabHand); }); //disengage for all interactions

        lastInstant = null;
        interactionList = null;
        //lastGrab = null;
        grabHand = null;
    }

    protected override void Engage()
    {
        var vis = true;
        RectTransform rect;
        if (eventCam != null &&  (rect = GetComponent<RectTransform>()) != null)
        {
            vis = IsVisibleFrom(rect, eventCam);
        }
        if (GrabbingHands.Count < 1 || !vis ) return; //if we're not grabbing yet
        lastInstant = Instantiate(spawnPrefab);
        lastInstant.transform.position = transform.position;
        
        grabHand = GrabbingHands[0].Hand;

        //Singular version
        //lastGrab = lastInstant.GetComponentInChildren<GrabInteraction>();
        //lastGrab.OnGrabEngaged(grabHand);

        //Applied to all interactions on the object
        interactionList = lastInstant.GetComponentsInChildren<Interaction>().ToList();
        interactionList.ToList().ForEach(x => x.OnGrabEngaged(grabHand)); //engage for all interactions      
    }

    protected override void Manipulate()
    {
        
    }

    private int CountCornersVisibleFrom(RectTransform rectTransform, Camera camera)
    {
        Rect screenBounds = new Rect(0f, 0f, Screen.width, Screen.height); // Screen space bounds (assumes camera renders across the entire screen)
        Vector3[] objectCorners = new Vector3[4];
        rectTransform.GetWorldCorners(objectCorners);

        int visibleCorners = 0;
        Vector3 tempScreenSpaceCorner; // Cached
        for (var i = 0; i < objectCorners.Length; i++) // For each corner in rectTransform
        {
            tempScreenSpaceCorner = camera.WorldToScreenPoint(objectCorners[i]); // Transform world space position of corner to screen space
            if (screenBounds.Contains(tempScreenSpaceCorner)) // If the corner is inside the screen
            {
                visibleCorners++;
            }
        }
        return visibleCorners;
    }

    private bool IsFullyVisibleFrom(RectTransform rectTransform, Camera camera)
    {
        return CountCornersVisibleFrom(rectTransform, camera) == 4; // True if all 4 corners are visible
    }

    private bool IsVisibleFrom(RectTransform rectTransform, Camera camera)
    {
        return CountCornersVisibleFrom(rectTransform, camera) > 0; // True if any corners are visible
    }
}
