﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOutOnLoad : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Invoke("DoFade", 1F);
	}

    private void DoFade()
    {
        GraphicFader fade = GetComponentInChildren<GraphicFader>();
        fade.FadeOutAfterDelay();
    }
}
