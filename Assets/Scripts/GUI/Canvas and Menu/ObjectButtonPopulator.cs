﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectButtonPopulator : MonoBehaviour {

    [SerializeField]
    private GameObject spawnLocation;

    [SerializeField]
    private GameObject buttonPrefab;


    public GameObject[] spawnablePrefabs;
    public string[] spawnablePrefabNames;



	// Use this for initialization
	void Start () {
		
        for(int i = 0; i < spawnablePrefabs.Length; i++)
        {
            if (spawnablePrefabs[i] == null) continue;


            GameObject button = Instantiate(buttonPrefab, transform);

            SpawnObject spawner = button.GetComponent<SpawnObject>();
            Text text = button.GetComponentInChildren<Text>();

            spawner.toSpawn = spawnablePrefabs[i];
            spawner.spawnLocation = spawnLocation;
            
            if(i >= spawnablePrefabNames.Length || spawnablePrefabNames[i] == null)
            {
                text.text = spawnablePrefabs[i].name;
            } else
            {
                text.text = spawnablePrefabNames[i];
            }
                       
        }
	}
	
}
