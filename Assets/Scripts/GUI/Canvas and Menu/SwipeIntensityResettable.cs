﻿using Meta.HandInput.SwipeInteraction;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeIntensityResettable : MonoBehaviour
{
    [Tooltip("Component to track hand position along a single axis")]
    [SerializeField]
    private LinearSwipeMonitor _monitor;

    private float _intensity = 0f;

    private bool swiping = false;

    /// <summary>
    /// Intensity raning from -1 to 1
    /// </summary>
    public float Intensity
    {
        get { return _intensity; }
    }

    /// <summary>
    /// True if a swipe interaction is occuring 
    /// </summary>
    public bool SwipeOn
    {
        get { return _monitor.Engaged; }
    }

    private void Update()
    {
        if (_monitor.Engaged)
        {
            float newIntensity = _intensity + _monitor.DeltaX;
            _intensity = Mathf.Clamp(newIntensity, -1f, 1f);
            swiping = true;
        } else if (!_monitor.Engaged && swiping)
        {
            swiping = false;
            _intensity = 0f;
        }
    }
}
