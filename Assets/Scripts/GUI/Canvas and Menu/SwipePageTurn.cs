﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Meta.HandInput.SwipeInteraction;

public class SwipePageTurn : MonoBehaviour {


    [SerializeField]
    private SwipeIntensityResettable _swipeIntensity;

    [SerializeField]
    private GameObject pagePanel;

    public float swipeTime = .4F;


    private bool startOfSwipe = false;
    private float startIntensity;
    private float lastIntensity;

    private int offset = 0;
    private int moveAmount = 150;
	
	// Update is called once per frame
	void Update () {
        if (_swipeIntensity.SwipeOn)
        {
            Debug.Log("Intensity: " + _swipeIntensity.Intensity);
            if (!startOfSwipe) {
                startOfSwipe = true;
                startIntensity = _swipeIntensity.Intensity;
            }
            lastIntensity = _swipeIntensity.Intensity;


        } else if (startOfSwipe)
        {
            startOfSwipe = false;
            float magnitude = lastIntensity - startIntensity;

            Debug.Log(magnitude);

            //swipe right
            if(magnitude > .25)
            {
                SwipeRight();
            }//swipe left
            else if (magnitude < -.25)
            {
                SwipeLeft();
            }

        }
    }

    void SwipeLeft()
    {
        if (offset > moveAmount * 10) return;
        else
        {
            offset += moveAmount;
            StartCoroutine(MoveToPosition(pagePanel.transform, pagePanel.transform.localPosition + Vector3.left * moveAmount, swipeTime));
        }
    }


    void SwipeRight()
    {
        if (offset <= 0) return;
        else
        {
            offset -= moveAmount;
            //pagePanel.transform.localPosition -= Vector3.left * moveAmount;
            StartCoroutine (MoveToPosition(pagePanel.transform, pagePanel.transform.localPosition - Vector3.left * moveAmount, swipeTime));
        }
    }


    public IEnumerator MoveToPosition(Transform transform, Vector3 position, float timeToMove)
    {
        var currentPos = transform.localPosition;
        var t = 0f;
        while (t < 1)
        {
            t += Time.deltaTime / timeToMove;
            transform.localPosition = Vector3.Lerp(currentPos, position, t);
            yield return null;
        }
    }

}
