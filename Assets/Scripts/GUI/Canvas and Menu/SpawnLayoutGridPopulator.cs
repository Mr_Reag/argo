﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;

[ExecuteInEditMode]
public class SpawnLayoutGridPopulator : MonoBehaviour {

    [SerializeField]
    private Camera eventCamera;

    [SerializeField]
    private GameObject[] prefabsToSpawn;

    [SerializeField]
    private string[] prefabNames;

    [SerializeField]
    private GameObject uiPrefab;

    [SerializeField]
    private bool update = false;

    int currentSize;


	// Use this for initialization
	void Start () {
        currentSize = prefabsToSpawn.Length;
        RepopulateContent();
	}
	
	// Update is called once per frame. Used for in editor execution
	void Update () {
		if(prefabsToSpawn.Length != currentSize || update)
        {
            currentSize = prefabsToSpawn.Length;
            RepopulateContent();
            update = false;
        }
	}


    private void RepopulateContent()
    {
        //Remove all children
        for(int i = 0; i < transform.childCount;i++)
        {
            var t = transform.GetChild(i);
            t.gameObject.SetActive(false);
            StartCoroutine(DestroyDelay(t.gameObject));
            
        }
        Debug.Log(transform.childCount);






        //Spawn in prefabs

        for(int i = 0; i < prefabsToSpawn.Length;i++)
        {
            var g = Instantiate(uiPrefab, transform);
            g.GetComponent<CloneGrabInteraction>().spawnPrefab = prefabsToSpawn[i];
            g.GetComponent<CloneGrabInteraction>().eventCam = eventCamera;

            string name = prefabsToSpawn[i].name;
            if(prefabNames.Length > i && prefabNames[i] != "")
            {
                name = prefabNames[i];
            }
            g.GetComponentInChildren<TextMeshProUGUI>().text = name;
        }

    }

    private IEnumerator DestroyDelay(GameObject g)
    {
        yield return new WaitForSecondsRealtime(.5F);
        DestroyImmediate(g);
    }
}
