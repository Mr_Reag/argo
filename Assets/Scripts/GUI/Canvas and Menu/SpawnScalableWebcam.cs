﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnScalableWebcam : MonoBehaviour {

    public int cameraToSpawn = -1;

    public GameObject webcamCanvasPrefab;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void OnClick()
    {
        if (cameraToSpawn >= WebCamTexture.devices.Length)
        {
            Debug.Log("Attempted to spawn invalid camera!");
            return;
        }

        GameObject spn = Instantiate(webcamCanvasPrefab);

        WebcamStreamer stream = spn.GetComponentInChildren<WebcamStreamer>();
        stream.webcamIndex = cameraToSpawn;
    }
}
