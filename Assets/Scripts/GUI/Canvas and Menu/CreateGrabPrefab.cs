﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CreateGrabPrefab : MonoBehaviour{

    public static GameObject grabbablePrefab;

    public static void CreateGrabbablePrefab(Transform parent, GameObject prefabToSpawn, Vector3 spawnScale)
    {
        //Instantiate the stand prefab
        GameObject grabPrefab = Instantiate(grabbablePrefab, parent);
        //grabPrefab.transform.position = parent.position;
        grabPrefab.transform.localScale = 75F * Vector3.one;

        //Set the CloneGrab to spawn our prefabToSpawn
        CloneGrabInteraction interaction = grabPrefab.GetComponentInChildren<CloneGrabInteraction>();
        interaction.spawnPrefab = prefabToSpawn;

        GameObject mini = Instantiate(prefabToSpawn, grabPrefab.transform);
        
        

        //Remove all non mesh components from the prefab
        Component[] cmplst = mini.GetComponentsInChildren<Component>();

        List<Component> lst = cmplst.ToList().Where(i =>
        (!i.GetType().Equals(typeof(MeshFilter))) && !i.GetType().Equals(typeof(Transform)) && (!i.GetType().Equals(typeof(MeshRenderer)))).ToList();

        lst.ForEach(i => Destroy(i));

        //mini.transform.localScale = spawnScale;

    }


    /*
    //Get the mesh components of our prefabToSpawn
    Mesh displayMesh = prefabToSpawn.GetComponent<MeshFilter>().sharedMesh;
    MeshRenderer displayRender = prefabToSpawn.GetComponent<MeshRenderer>();

    //Set our preview mesh to our saved components
    GameObject prefabMesh = grabPrefab.transform.Find("PrefabMesh").gameObject;
    prefabMesh.GetComponent<MeshFilter>().mesh = displayMesh;
    MeshRenderer prefabRend = prefabMesh.GetComponent<MeshRenderer>();
    if(prefabRend != null)
    {
        Destroy(prefabRend);
    }
    var r = prefabMesh.AddComponent<MeshRenderer>();
    r = displayRender;
    */
}

