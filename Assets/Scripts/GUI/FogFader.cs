﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FogFader : MonoBehaviour {

    public Image img;

    [HideInInspector]
    public bool doneFading = true;

    private void Start()
    {
        if (img == null)
            Destroy(this);
    }


    public void FadeFogOut()
    {
        //Debug.Log("FogFadeOut");
        StartCoroutine(FadeImage(true));
    }

    public void FadeFogIn()
    {
        //Debug.Log("FogFadeIn");
        StartCoroutine(FadeImage(false));
    }

    public void RestoreFog()
    {
        //Debug.Log("FogRestored");
        img.CrossFadeAlpha(1, 0F, true);
        doneFading = true;
    }

    public void RemoveFog()
    {
        //Debug.Log("FogRemoved");
        img.CrossFadeAlpha(1, 0F, true);
        doneFading = true;
    }

    IEnumerator FadeImage(bool fadeOut)
    {
       // Debug.Log("Fade started: " + fadeOut);
        doneFading = false;
        // fade from opaque to transparent
        if (fadeOut)
        {
            img.CrossFadeAlpha(0, .5F, true);
            yield return new WaitForSecondsRealtime(.5F);
        }
        // fade from transparent to opaque
        else
        {
            img.CrossFadeAlpha(1, .5F, true);
            yield return new WaitForSecondsRealtime(.5F);
        }
        doneFading = true;
       // Debug.Log("Fade Ended"+ fadeOut);
    }
}
