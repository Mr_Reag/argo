﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PageController : MonoBehaviour {

    private int pageNum;



	// Use this for initialization
	void Start () {
        pageNum = 0;
        if (gameObject.transform.childCount <= 0) return;

        for(int i=0; i < gameObject.transform.childCount; i++)
        {
            Transform curr = gameObject.transform.GetChild(i);
            curr.gameObject.SetActive(false); //turn off all other pages
        }
        gameObject.transform.GetChild(0).gameObject.SetActive(true); //render first page
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    


    public void NextPage()
    {
        if(pageNum+1 >= gameObject.transform.childCount)
        {
            return;
        }

        Transform currPg = gameObject.transform.GetChild(pageNum);
        currPg.gameObject.SetActive(false);
        pageNum++;
        Transform nxtPg = gameObject.transform.GetChild(pageNum);
        nxtPg.gameObject.SetActive(true);

    }


    public void PreviousPage()
    {
        if (pageNum == 0)
        {
            return;
        }

        Transform currPg = gameObject.transform.GetChild(pageNum);
        currPg.gameObject.SetActive(false);
        pageNum--;
        Transform nxtPg = gameObject.transform.GetChild(pageNum);
        nxtPg.gameObject.SetActive(true);
    }

}
