﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateWebcamButtonLayout : MonoBehaviour {


    public GameObject webcamButtonPrefab;

	// Use this for initialization
	void Start () {
        WebCamDevice[] devices = WebCamTexture.devices;

        for(int i=0; i < devices.Length;i++)
        {
            Instantiate(webcamButtonPrefab, transform);

            SpawnScalableWebcam cam = webcamButtonPrefab.GetComponentInChildren<SpawnScalableWebcam>();
            cam.cameraToSpawn = i;
            Text buttonText = cam.GetComponentInChildren<Text>();
            buttonText.text = "Spawn View For " + devices[i].name;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
