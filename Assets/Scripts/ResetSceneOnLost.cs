﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetSceneOnLost : MonoBehaviour {



    void Start()
    {

        InvokeRepeating("DistanceCheck", 2F, 2F);
    }

    void DistanceCheck()
    {
        Vector3 playerPos = Camera.main.transform.position;
        Vector3 distanceVec = playerPos - transform.position;
        double mag = distanceVec.magnitude;
        if (mag > 100)
        {
            ResetScene();
        }
    }

    void ResetScene()
    {
        ResetController.ResetScene();
    }
}
