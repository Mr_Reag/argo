﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Meta;

public class GrabRotateOnInteraction : MonoBehaviour {

    private bool rotate = false;



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(rotate)
        {
            /*Vector3 toLook = new Vector3(
            Camera.main.transform.forward.x,
            transform.forward.y,
            Camera.main.transform.forward.z); //keep x and z the same, but change the y vector
            */
            transform.forward = Camera.main.transform.forward;
        }
	}


    public void OnEngage(MetaInteractionData data)
    {
        rotate = true;
    }

    public void OnDisengage(MetaInteractionData data)
    {
        rotate = false;
    }

}
