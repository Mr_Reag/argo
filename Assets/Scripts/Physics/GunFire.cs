﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunFire : MonoBehaviour {

    [SerializeField]
    private float cooldown;
    [SerializeField]
    private GameObject projectile;
    [SerializeField]
    private float force;
    [SerializeField]
    private GameObject firePoint;

    private bool onCooldown = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(!onCooldown && Input.GetKeyDown("space"))
        {
            onCooldown = true;
            Invoke("ResetCooldown", cooldown);
            FireProjectile();
        }
	}

    public void FireProjectile()
    {
        GameObject proj = Instantiate(projectile);
        proj.transform.position = firePoint.transform.position; //make our projectile spawn at the firepoint
        Vector3 plusz = firePoint.transform.TransformPoint(firePoint.transform.localPosition + new Vector3(0,0,1)); //get a point 1z away on the local
        Vector3 fireDir = plusz - firePoint.transform.position; //subtract our +z from the original pos to get our direction
        Rigidbody rb = proj.GetComponent<Rigidbody>();
        if(rb == null)
        {
            rb = proj.AddComponent<Rigidbody>();
        }
        rb.useGravity = true;
        rb.isKinematic = false;
        rb.AddForce(fireDir * force, ForceMode.Impulse);
    }

    private void ResetCooldown()
    {
        onCooldown = false;
    }
}
