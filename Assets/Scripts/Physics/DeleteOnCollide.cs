﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Meta;

public class DeleteOnCollide : MonoBehaviour {



    static List<string> whitelistNames;
    
	// Use this for initialization
	void Start () {
        whitelistNames = new List<string>
        {
            "Meta", //We dont want to delete any meta objects
            "Hand",
            "Canvas",
            "Reset",
            "Menu",
            "Pointer",//or the canvas!
            "PlanPoint" //Also lets not delete the location Markers
        };
    }
	
	// Update is called once per frame
	void Update () {
		
	}


    private void OnTriggerEnter(Collider other)
    {
        DeleteObject(other);
    }


    private void OnTriggerStay(Collider other)
    {
        DeleteObject(other);
    }


    private void DeleteObject(Collider other)
    {
        GameObject master = other.transform.root.gameObject; //get the highest level parent

        //Debug.Log("Basket collided with " + master.name);


        //See if the object is in our whitelist. Dont delete it if it is
        if (whitelistNames.Count(x => master.name.IndexOf(x, System.StringComparison.OrdinalIgnoreCase) >= 0) != 0)
        {
            return;
        }
        MetaDelete.DeleteMetaObject(master);
    }
}
