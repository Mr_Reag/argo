﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeMaster : MonoBehaviour {

    public int numSections;
    public GameObject ropePrefab;


    private GameObject lastLink = null;
    //public float ropeSectionSize = .022F;
    //public float rotationY = 90F;

    private int linkCount = 0;



    // Use this for initialization
    void Start () {
        lastLink = this.gameObject;

        for (int i = 0; i < numSections; i++)
        {
            AddLink();
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp("w"))
            AddLink();
    }



    public void AddLink()
    {
        GameObject section = Instantiate(ropePrefab, gameObject.transform);
        Vector3 offset = ropePrefab.transform.position;
        section.name = section.name + "_" + ++linkCount;


        section.transform.localPosition = lastLink.transform.localPosition;
        section.transform.localPosition += offset;

        Joint hinge = section.GetComponent<Joint>();

        
        hinge.connectedBody = lastLink.GetComponent<Rigidbody>();


        lastLink = section;
    }
}
