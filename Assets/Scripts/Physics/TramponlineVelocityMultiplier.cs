﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TramponlineVelocityMultiplier : MonoBehaviour {

    public float multiplier = 1.5F;


    private bool cooldown = false;


	// Use this for initialization
	void Start () {
		
	}


    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Collider on " + gameObject.name + " with " + other.transform.name);
        if (cooldown) return;
        var rb = other.gameObject.GetComponentInChildren<Rigidbody>();
        ReflectRB(rb);
    }


    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Collion on " + gameObject.name + " with " + collision.transform.name);
        if (cooldown) return;
        
        var rb = collision.gameObject.GetComponentInChildren<Rigidbody>();
        ReflectRB(rb);
    }


    private void ReflectRB(Rigidbody rb)
    {
        if (rb == null) return;
        if (rb.isKinematic || !rb.useGravity) return;

        Invoke("ResetCooldown", .1F);
        cooldown = true;

        Vector3 velocity = rb.velocity;
        Debug.Log("Velocity is " + velocity.ToString());
        Vector3 normal = transform.root.transform.up.normalized;
        Debug.Log("Normal is " + normal.ToString());
        Vector3 reflection = Vector3.Reflect(velocity, normal);
        Debug.Log("Reflected is " + reflection.ToString());
        reflection *= multiplier;

        rb.velocity = reflection;
    }



    void ResetCooldown()
    {
        cooldown = false;
    }




}
