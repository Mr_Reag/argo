﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public static class MeshUtilities {




    public static Dictionary<Vector3,Vector3> PickRandomVerticiesAndNormals(Mesh mesh, float percent)
    {
        var map = new Dictionary<Vector3, Vector3>();
        System.Random rand = new System.Random();
        Debug.Log("Processing " + mesh.vertexCount + " vertices");
        for(int i = 0; i < mesh.vertexCount; i++)
        {
            if(rand.NextDouble()*100 < percent)
            {
                //Add the worldspace position of the vertices, and their normals
                if (!map.ContainsKey(mesh.vertices[i]))
                    map.Add(mesh.vertices[i], mesh.normals[i]);
            }
        }

        return map;
    }


    public static void PlacePrefabAtLocations(Dictionary<Vector3, Vector3> vectorNormals, GameObject owner, GameObject prefab)
    {
        PlacePrefabAtLocations(vectorNormals, owner, prefab, 1F);
    }

    public static void PlacePrefabAtLocations(Dictionary<Vector3,Vector3> vectorNormals,GameObject owner, GameObject prefab, float scale)
    {
        Dictionary<Vector3,Vector3>.KeyCollection keys = vectorNormals.Keys;
        foreach(Vector3 loc in keys)
        {
            GameObject obj = UnityEngine.Object.Instantiate(prefab);
            obj.transform.localScale *= scale;
            obj.transform.localPosition = owner.transform.TransformPoint(loc);
            obj.transform.rotation = Quaternion.LookRotation(vectorNormals[loc]);
            obj.transform.Rotate(90,0,0);
        }

    }


    //Simple testing script
    public static void ColorVerticies(ref Mesh mesh, List<Vector3> toColor, Color color)
    {
        Vector3[] vert = mesh.vertices;
        for(int i=0;i<vert.Length;i++)
        {
            if(toColor.Contains(vert[i]))
            {
                mesh.colors32[i] = color;
            }
        }
    }


    public static void DeleteVerticies(ref Mesh mesh, List<Vector3> toDelete)
    {
        List<Vector3> newVert = new List<Vector3>();
        List<int> newTri = new List<int>();
        int[] triMap = new int[mesh.triangles.Length];

        int count = 0;

        for (int i = 0; i < mesh.vertices.Length; i++)
        {
            if(toDelete.Contains(mesh.vertices[i]))
            {
                triMap[i] = -1;
                Debug.Log("Deleting vert " + i);
            } else
            {
                triMap[i] = count;
                count++;
                newVert.Add(mesh.vertices[i]);
            }
        }

        int[] tri = mesh.triangles;

        for(int i = 0; i < tri.Length;i+=3)
        {
            if(triMap[tri[i]] == -1 || triMap[tri[i +1]] == -1 || triMap[tri[i +2]] == -1)
            {
                Debug.Log("Removing tri num:" + i + ","+(i+1)+"," + (i + 2));
                continue;
            } else
            {
                newTri.Add(triMap[tri[i]]);
                newTri.Add(triMap[tri[i+1]]);
                newTri.Add(triMap[tri[i+2]]);
            }
        }
        mesh.Clear();
        mesh.vertices = newVert.ToArray();
        mesh.triangles = newTri.ToArray();

        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
        mesh.RecalculateTangents();
    }

    private static void DeleteVertex(ref Mesh mesh, Vector3 vert)
    {
        Debug.Log("Old vert count: " + mesh.vertices.Length);
        //Find the vert to delete
        int keyIndex = Array.FindIndex(mesh.vertices, x => x.Equals(vert));
        Debug.Log("Key Index: " + keyIndex);
        Vector3[] newVert = new Vector3[mesh.vertices.Length-1];
        Debug.Log("New vert count: " + newVert.Length);

        for(int i=0; i < newVert.Length;i++)
        {
            newVert[i] = (i >= keyIndex) ? mesh.vertices[i+1]: mesh.vertices[i] ;
        }

        //find the triangles to delete
        int[] tri = new int[mesh.triangles.Length];
        Array.Copy(mesh.triangles, tri,tri.Length);

        Debug.Log("Old Tri Count: " + tri.Length);

        for(int i=0; i < mesh.triangles.Length;i+=3)
        {
            //if our deleted vertex is part of a triangle
            if (tri[i] == keyIndex || tri[i + 1] == keyIndex || tri[i + 2] == keyIndex)
            {
                //mark for removal
                tri[i] = tri[i + 1] = tri[i + 2] = -1;
                Debug.Log("Removing trigangle " + i);
            } 
        }
        int[] newTri = tri.Where(x => x != -1).ToArray();
        //shift triangle references down

        for(int i=0; i < newTri.Length;i++)
        {
            if (newTri[i] > keyIndex) newTri[i]--;
        }

        Array.ForEach(newTri, delegate (int i) { Debug.Log(i); });

        mesh.Clear();
        mesh.vertices = newVert;
        mesh.triangles = newTri;
    }

    

}
