﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillboardSprite : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void LateUpdate()
    {

        /*Vector3 toLook = new Vector3(
            Camera.main.transform.forward.x,
            transform.forward.y,
            Camera.main.transform.forward.z); //keep x and z the same, but change the y vector

        transform.forward = toLook;*/



        //transform.forward = Camera.main.transform.forward;
        /*
        var target = Camera.main.transform.position;
        target.y = -transform.position.y;

        //target = target * -1;

        transform.LookAt(target);
        */

        var target = Camera.main.transform.position;
        target.y = transform.position.y;
        transform.LookAt(target);
        transform.Rotate(0, 180, 0);
    }
}
